# For polycubes

def f(n):
    assert(n >= 0)
    if n <= 11: return (2 * n + 2) // 3
    elif n <= 26: return (2 * n + 3) // 3
    elif n <= 40: return (2 * n + 4) // 3

def g(n):
    assert(n >= 0)
    if n == 1: return 0
    elif n <= 25 and n % 6 == 1 or n == 6: return f(n) + 1
    elif n <= 40: return f(n)
    elif n <= 81: return f(n - 41) + 28
    else: return g(n - 41) + 28

def gap(d):
    differences = set(g(n) - g(n - d) for n in range(500, 1000))
    min_d = min(differences)
    n = 500
    while n >= d and g(n) - g(n - d) >= min_d:
        n -= 1
    n += 1
    return min_d

def xgap(d):
    differences = set(g(n) - g(n - d) for n in range(500, 1000))
    min_d = min(differences)
    n = 500
    while n >= d and g(n) - g(n - d) >= min_d:
        n -= 1
    n += 1
    return (min_d, n)

# For polyominos

def g2(n):
    assert(n >= 0)
    if n == 0 or n == 1: return 0
    elif n == 2: return 2
    elif n <= 5: return n - 1
    else: return g2(n - 4) + 2

def gap2(d):
    differences = set(g2(n) - g2(n - d) for n in range(500, 1000))
    min_d = min(differences)
    n = 500
    while n >= d and g(n) - g(n - d) >= min_d:
        n -= 1
    n += 1
    return min_d
