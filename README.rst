Fully leafed tree polycubes
~~~~~~~~~~~~~~~~~~~~~~~~~~~

This respository contains code used to generate fully leafed branches of tree
polycubes. The program included was used in an article titled *Fully Leafed
Tree-Like Polyominoes and Polycubes*, submitted to the 29th Conference on
Formal Power Series and Algebraic Combinatorics (FPSAC 2017), hosted in London.

It definitely needs a cleanup, but it can be directly use to reproduce the
results. Notice that it took more than **24 hours** of computation on an iMac
to obtain empty all branches up to depth 10 (there are no alive branches of
depth 11 and more).

Dependencies
============

- `Sagemath <http://www.sagemath.org/>`__. One needs to install Sagemath to
  make it work.

How to use
==========

Once Sagemath is installed, it suffices to enter the command::

    $ sage cubes_set.spyx <depth>

where ``depth`` is the depth of the branches. For instance, we get::

    $ sage cubes_set.spyx 2
    Compiling cubes_set.spyx...
    k = 1
    [5]
    [6]
    [5]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [2, 6]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [3, 5]
    *** Not pruned: [3, 5, 5]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [3, 5]
    *** Not pruned: [3, 5, 5]
    *** Pruned by cut: [2, 5]
    *** Pruned by cut: [3, 5]
    k = 2
    [3, 5, 5]

which also produces ``.json`` files indicating what branches are alive or
eliminated, either by substitution or cut.

License
=======

All files in this repository are subject to the `GPLv3 license
<https://www.gnu.org/licenses/gpl-3.0.en.html>`__.
